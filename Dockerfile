FROM registry-gitlab.pasteur.fr/tru/docker-ubuntu-lts18.04-ci:latest
MAINTAINER Tru Huynh <tru@pasteur.fr>

RUN apt-get update && DEBIAN_FRONTEND=noninteractive  apt-get -y upgrade
# munge and requirements for /pasteur/sonic/hpc/slurm/maestro/slurm/bin/scontrol
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install munge libreadline7

RUN date +"%Y-%m-%d-%H%M" > /last_update
