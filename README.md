# building a minimal image based on Ubuntu LTS 18.04 for CI (based on the singularity recipe at https://gitlab.pasteur.fr/tru/ubuntu-lts18.04-maestro)

Tru <tru@pasteur.fr>

(toy) docker image produced available at `registry-gitlab.pasteur.fr/tru/docker-ubuntu-lts18.04-maestro-ci:latest`
